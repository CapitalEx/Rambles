# Stack Effects for the Unfamiliar [Tangent]

Stack effects for the unfamiliar are the Forthism for function arguments. In a
language like Forth, they are just comments used for documentation. In a
language like Factor, they are checked and used for optimizations. In a statically
typed language, they also validate inputs to functions. They are equivalent to
function declariations in applicative languages:

```
fn foo(i32, i32) -> i32 
    
: foo ( i32 i32 -- i32 ) 
```

Additionally, stack languages implicitly have multiple return values:

```
fn split(vector3) -> (int, int, int)

: split ( vector3 -- int int int )
```

The best way to view stack effects is showing how the function will change the
stack:

```
: f ( before -- after )
```

Like, other languages these can be made generic:

```
fn dup<A>(A) -> (A, A)

: dup ( 'a -- 'a 'a )
```

However, unlike other languages, the *number* of outputs and inputs can also be
made generic. This allows for combinators to take any function and apply them to
the stack. For example `dip` which takes a quote and calls under the top of
the stack:

```
: dip ( ..a 'x ( ..a -- ..b ) -- ..b )
```

Languages like Haskell or Rust have no way to express this kind of relationship.
So, stack based languages cannot be directly embedded inside them. A dependently
typed language like Idris could represent them through type-level programming.

But generic stack effects introduce some complexities.

# Generic Stack Effects

Generic stack effects are useful for expressing how combinators interact with
the state of the program. For example, Factor's `bi` combinator can be described
as followed:

```factor
: bi* ( ..a 'x ( ..a -- ..b ) ( ..b 'x -- ..c ) -- ..c )
    [ dip ] dip call ;
```

However, some issues come up reatlively quickly. The second quotation has the
type `( ..b 'x -- ..c )`. Without some way to re-write the stack effect, this
function wouldn't be able to process the following:

```
: pass ( -- ) ;

"foobar" [ 10 ] [ pass ] bi
```
The result of running this in Factor will be `10 "foobar"` on the stack (where 
`'x` is any value). However, Factor utilizes it's `inline` keyword to get around
the stack checker. But even if `inline` wasn't needed, a simple stack checker
would be unhappy.

It's clearly a valid operation, but `pass` doens't quite fit the effect
specified: ( ..b 'x -- ..c ). If we go through this value by value, we can see
where things might go wrong. At each step the stack is denoted to the left of 
the arrow:

```
"foobar" => { string }
[ 10 ]   => { string ( -- int ) }
[ pass ] => { string ( -- int ) ( -- ) }
bi*      => ( string ( -- int ) ( int string -- ) -- ) {{ Huh? }}
```

Wait, that... that last time doens't look right? `[ pass ]` can't be called by
be in this context. But, it should be runnable. Let's go step by step:

- `..a` is replaced by an empty stack from thel left side of `( -- int )`
- `'x` is replaced by `string` from `"foobar"` that gets pushed on eariler
- `..b` is replaced by `int` from the right side of `( -- int )`

Putting this all together we end up with `bi` needing a quotation of type
`( int string -- )`. However, this kinda renders the combinator useless. `pass`
obviously doesn't change the stack. So  it shoud be inferred as:

```
( string int -- string int )
```
which then results in the following type:
```
( string ( -- int ) ( int string -- int string ) -- int string ) {{ Nice. }}
```

But this requies giving the stack checker the ability to re-write stack effects
in a couple ways.



## Implicit Argument Variable

All stack effects can be upgraded by providing an implicit argument variable. 
This variables are always added in balanced pairs. This allows the stack checker
to preform the following upgrades:

```
( ... -- ... ) => ( 'x ... -- 'x ... ) => ( 'y 'x ... -- 'y 'x ... ) => etc.
```

Give the following functions:
```
: f ( a -- )
: g ( b -- )
: h ( c -- )
: i ( d -- )
```
we can interpet the statement `f g h i` in two ways:
```
d c b a => ( a   -- ) 
  d c b => ( b   -- ) 
    d c => ( c   -- )
      d => ( d   -- )
-- empty --

  d c b a 
( d c b a -- d c b )
           ( d c b  --  d c )
                      ( d c -- d )
                             ( d -- ) 
                      
```

The first interpretation, each call can be thought of as simply consuming values
from the stack. In the second interpretation, each call can be thought of as
transforming the state of the full stack. Both views are functionally the same.
The compiler and stack checker will enforce the definitions provided. This
insures that `f`, `g`, and `h` will not consume, read, or mutate values they 
have no access to.

## Implicit Stack Variable

All stack effects can be upgraded by providing an implicit stack variable. This
variable must come after all other inputs. This generalization asserts that all 
functions leave the part of the stack they don't access unchanged. 
For example, the following upgrades may be preformed:

```
(    --    ) => ( ..a    -- ..a    )
( 'x --    ) => ( ..a 'x -- ..a    )
(    -- 'x ) => ( ..a    -- ..a 'x )
( 'x -- 'y ) => ( ..a 'x -- ..a 'y )
```

## Mix and Match

These two re-writing strategies can be combine together to produce more 
complex types from the inputs. The type check may treat `( 'x 'y -- 'z )`
as `( ..a 'b 'x 'y -- ..a 'b 'z )` when checking result. Or, it may expand 
`( -- )` into `( 'y 'x -- 'y 'x )`. This is give the stack check the flexiblity 
it needs to account for combinators.
