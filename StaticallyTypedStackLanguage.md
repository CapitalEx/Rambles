# Statically Typed Stack-Based Language
The following is a stream of thought for a statically typed programming language. The language passes all data between functions using the stack.
It is largely inspired by Factor borrowing one idea (see: Typed Holes) from Idris.

## Type Signatures
The following are some basic type signatures. Mostly just some quick ideas for them.
```factor
:> +   > Num Num  --  Num ; @primitive 
:> *   > Num Num  --  Num ; @primitive
:> -   > Num Num  --  Num ; @primitive
:> /   > Num Num  --  Num ; @primitive
:> ?   > Bool a a --    a ;
:> reverse > String -- String ;
```
The word `?` will be important later. The gist is that it takes a boolean
and two values of the same type. If true, it returns the left-hand side.
if false, it returns the right-hand side.

## Stack Generic Operations
Some words will need to assume the types that they have available. In this case,
the syntax Factor uses for "row polymorphism" is extended. The type `...` is
automatically inferred based on the other types in the signature. It is required
to appear in at least 1 quotes as an input. That is used to infer its type.

```factor
:> bi  > ..a     ( ..a -- ..c ) ( ..a -- ..b ) -- ..c ..b ;
:> bi@ > ..a ..a ( ..a -- ..b )                -- ..b ..b ;
:> bi* > ..a ..b ( ..a -- ..c ) ( ..b -- ..d ) -- ..c ..d ;
```

Word defintion is silimar to Factor. Stack effects in this context merely server
as guides for the reader. Since we are using static typing, we already declare
the stack effects. Thus their usage of them may be optional.
```Factor
:: bi       ( ... q   p -- ..q ..p ) [ keep ] dip call ;
:: bi*      ( ... q   p -- ..q ..p )  [ dip ] dip call ;
:: bi@      ( ... ... q -- ..q ..q )           dup bi* ;
:: ?        ( ?   x   y -- x/y     )               ... ;
:: reverse  ( str       -- str     )               ... ;
```

This allows for compile-time checking of operations.

```factor
"Hello" [ sq ] bi@ ! Produces a type error at compile time
     10 [ sq ] bi@ ! runs fine

:: oops ( Integer -- Integer )   ! Won't compile
    reverse ;                   

:: nice ( Integer -- Integer )   ! Will compile
    cast! reverse cast! ; 
```
## Typed Holes
The following is an idea borrowed from Idris. Idris can define incomplete, but type-safe programs using "holes". For this hypothetical language, holes are implemented using the parsing word `Suggest?`.

```factor
:> is-palindrome > String -- String ;
:: is-palindrome ( str -- str )
    [ Suggest? word-1? ] 
    [ Suggest? word-2? ] bi "palindrome" "not-palindrome" ? ;
```

Suggest? is a parsing word that takes the next token in the stream
and marks it as a typed hole. This allows for an incomplete program
that is type-safe.

When processing a Suggest?, The Assistant works backward. It first
computes the types on the stack for the current word. Each quote is recursively explored to resolve their types.
After this operation, the Assistant would see the following stack
from the bottom to the top :

```nim
String
( String'a -- String'c ) ( ... String'a -- Bool'd ) -- Bool
String
String
Bool String String -- String
```

The user could then query solutions for `word-1?` and `word-2?`
if no existing word can satisfy `word-1?` or `word-2?`, a new word
can be generated. In the case of `word-2?` there is nothing that the
Assistant can suggest. However, `word-1?` can be replaced with
`reverse` if the programmer chooses to.

`...` can be used as a generic placeholder to say: "Use whatever types completely this stack-effect"

```factor
:> word-2 > ..a String -- Bool ;
:: word-2 ( ... str -- ? )
    word-2'? ;

:> is-palindrome > String -- String ;
:: is-palindrome ( str -- str )
    [ reverse ]
    [ word-2  ] bi "palindrome" "not-palindrome" ? ;
```
In this case, it would be just the type `String`. Additionally the 
The Assistant could specialize this type further so that it's more 
explicit:

```factor
:> word-2 > String String -- Bool ;
:: word-2 ( str str -- ? )
    Suggest? word-2'?
```

Now, the Assistant could tell that `word-2'?` needs to have the following
type: `String String -- Bool`. Assuming that string implements some 
interface for ordering, the Assistant could then suggest `=` in the
list of possible words.

```factor
:> word-2 > String String -- Bool ;
:: word-2 ( str str -- ? )
    = ;
```
The Assistant or the programmer could inline this simple word to remove
the aliasing of `=`.


## The Type Aware Assistant
The idea of the Type Aware Assistant is that it can use type information
to remove the need to sift through documentation.

Additionally, by implementing this in a stack-based programming language
the TAA always knows what data the programmer has access two.
Even if the idea of globals/local variables is introduced:

`some-var get-global Suggest? help?`

The requirement of placing data on the stack always makes the state transparent
to the TAA at any given point in the source code:
```factor
:> some-var   > [ T Symbol   ]
:> get-global > [ T Symbol   ] -- [ T Optional ]
:> help?      > [ T Optional ] -- ...
```

